FROM golang:1.17

WORKDIR /go/src/gitlab.com/sempiternalmonk/blobblob

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o /usr/local/bin/blob gitlab.com/sempiternalmonk/blobblob


###

FROM alpine:3.9

COPY --from=0 /usr/local/bin/blob /usr/local/bin/blob
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["blob"]
