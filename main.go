package main

import (
	"os"

	"gitlab.com/sempiternalmonk/blobblob/internal/cli"
)

func main() {
	if !cli.Run(os.Args) {
		os.Exit(1)
	}
}
