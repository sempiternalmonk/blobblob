/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

import "encoding/json"

type BlobAttributes struct {
	// blob identifier
	BlobId *string         `json:"blob-id,omitempty"`
	Value  json.RawMessage `json:"value"`
}
