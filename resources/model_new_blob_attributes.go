/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

import "encoding/json"

type NewBlobAttributes struct {
	Value json.RawMessage `json:"value"`
}
