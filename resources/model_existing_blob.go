/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type ExistingBlob struct {
	Key
	Attributes ExistingBlobAttributes `json:"attributes"`
}
type ExistingBlobResponse struct {
	Data     ExistingBlob `json:"data"`
	Included Included     `json:"included"`
}

type ExistingBlobListResponse struct {
	Data     []ExistingBlob `json:"data"`
	Included Included       `json:"included"`
	Links    *Links         `json:"links"`
}

// MustExistingBlob - returns ExistingBlob from include collection.
// if entry with specified key does not exist - returns nil
// if entry with specified key exists but type or ID mismatches - panics
func (c *Included) MustExistingBlob(key Key) *ExistingBlob {
	var existingBlob ExistingBlob
	if c.tryFindEntry(key, &existingBlob) {
		return &existingBlob
	}
	return nil
}
