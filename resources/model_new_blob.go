/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type NewBlob struct {
	Key
	Attributes NewBlobAttributes `json:"attributes"`
}
type NewBlobResponse struct {
	Data     NewBlob  `json:"data"`
	Included Included `json:"included"`
}

type NewBlobListResponse struct {
	Data     []NewBlob `json:"data"`
	Included Included  `json:"included"`
	Links    *Links    `json:"links"`
}

// MustNewBlob - returns NewBlob from include collection.
// if entry with specified key does not exist - returns nil
// if entry with specified key exists but type or ID mismatches - panics
func (c *Included) MustNewBlob(key Key) *NewBlob {
	var newBlob NewBlob
	if c.tryFindEntry(key, &newBlob) {
		return &newBlob
	}
	return nil
}
