/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type ResourceType string

// List of ResourceType
const (
	REGISTER_BLOB   ResourceType = "register-blob"
	RECEIVE_BLOB    ResourceType = "receive-blob"
	REGISTERED_BLOB ResourceType = "registered-blob"
)
