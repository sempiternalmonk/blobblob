package postgre

import (
	"database/sql"
	"fmt"

	sq "github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/sempiternalmonk/blobblob/internal/data"
)

const (
	blobsTableName    = "blobs"
	blobIDColumnName  = "blob_id"
	blobUIDColumnName = "owner_address"
)

func NewBlobsQ(db *pgdb.DB) data.BlobQ {
	return &blobsQ{
		db:  db.Clone(),
		sql: sq.Select("*").From(fmt.Sprintf("%s", blobsTableName)),
	}
}

type blobsQ struct {
	db  *pgdb.DB
	sql sq.SelectBuilder
}

func (q *blobsQ) Transaction(fn func(q data.BlobQ) error) error {
	return q.db.Transaction(func() error {
		return fn(q)
	})
}

func (q *blobsQ) New() data.BlobQ {
	return NewBlobsQ(q.db)
}

func (q *blobsQ) GetByID(blobID int32) (data.Blob, error) {
	var result data.Blob

	sql := q.sql.Where(sq.Eq{blobIDColumnName: blobID})
	//fmt.Println(sql.ToSql())
	if err := q.db.Get(&result, sql); err != nil {
		return result, err
	}
	return result, nil
}

func (q *blobsQ) Insert(blob data.Blob) (data.Blob, error) {
	var result data.Blob
	sql := sq.Insert(blobsTableName).SetMap(map[string]interface{}{
		"owner_address": blob.OwnerAddress,
		"json_blob":     blob.JsonBlob,
	}).Suffix("returning *")
	err := q.db.Get(&result, sql)

	return result, err
}

func (q *blobsQ) Delete(blobID int32) (sql.Result, error) {
	sql := sq.Delete(blobsTableName).Where(sq.Eq{blobIDColumnName: blobID})
	//fmt.Printf(sql.ToSql())
	res, err := q.db.ExecWithResult(sql)
	return res, err
}

func (q *blobsQ) SelectByUID(userID string) ([]data.Blob, error) {
	result := make([]data.Blob, 0)
	sql := q.sql.Where(sq.Eq{blobUIDColumnName: userID})
	//fmt.Println(sql.ToSql())
	if err := q.db.Select(&result, sql); err != nil {
		return nil, err
	}

	return result, nil
}
