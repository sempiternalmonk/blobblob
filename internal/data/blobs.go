package data

import "database/sql"

type Blob struct {
	OwnerAddress string `db:"owner_address,omitempty"`
	BlobID       int    `db:"blob_id,omitempty"`
	JsonBlob     string `db:"json_blob,omitempty"`
}

//mockery --name BlobQ --case underscore
type BlobQ interface {
	New() BlobQ
	Transaction(fn func(q BlobQ) error) error

	GetByID(blobID int32) (Blob, error)
	Delete(blobID int32) (sql.Result, error)
	Insert(blob Blob) (Blob, error)
	SelectByUID(userID string) ([]Blob, error)
}
