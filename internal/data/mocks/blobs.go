// Code generated by mockery v2.9.4. DO NOT EDIT.

package mocks

import (
	mock "github.com/stretchr/testify/mock"
	data "gitlab.com/sempiternalmonk/blobblob/internal/data"

	sql "database/sql"
)

// BlobQ is an autogenerated mock type for the BlobQ type
type BlobQ struct {
	mock.Mock
}

// Delete provides a mock function with given fields: blobID
func (_m *BlobQ) Delete(blobID int32) (sql.Result, error) {
	ret := _m.Called(blobID)

	var r0 sql.Result
	if rf, ok := ret.Get(0).(func(int32) sql.Result); ok {
		r0 = rf(blobID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(sql.Result)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(int32) error); ok {
		r1 = rf(blobID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetByID provides a mock function with given fields: blobID
func (_m *BlobQ) GetByID(blobID int32) (data.Blob, error) {
	ret := _m.Called(blobID)

	var r0 data.Blob
	if rf, ok := ret.Get(0).(func(int32) data.Blob); ok {
		r0 = rf(blobID)
	} else {
		r0 = ret.Get(0).(data.Blob)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(int32) error); ok {
		r1 = rf(blobID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Insert provides a mock function with given fields: blob
func (_m *BlobQ) Insert(blob data.Blob) (data.Blob, error) {
	ret := _m.Called(blob)

	var r0 data.Blob
	if rf, ok := ret.Get(0).(func(data.Blob) data.Blob); ok {
		r0 = rf(blob)
	} else {
		r0 = ret.Get(0).(data.Blob)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(data.Blob) error); ok {
		r1 = rf(blob)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// New provides a mock function with given fields:
func (_m *BlobQ) New() data.BlobQ {
	ret := _m.Called()

	var r0 data.BlobQ
	if rf, ok := ret.Get(0).(func() data.BlobQ); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(data.BlobQ)
		}
	}

	return r0
}

// SelectByUID provides a mock function with given fields: userID
func (_m *BlobQ) SelectByUID(userID string) ([]data.Blob, error) {
	ret := _m.Called(userID)

	var r0 []data.Blob
	if rf, ok := ret.Get(0).(func(string) []data.Blob); ok {
		r0 = rf(userID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]data.Blob)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(userID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Transaction provides a mock function with given fields: fn
func (_m *BlobQ) Transaction(fn func(data.BlobQ) error) error {
	ret := _m.Called(fn)

	var r0 error
	if rf, ok := ret.Get(0).(func(func(data.BlobQ) error) error); ok {
		r0 = rf(fn)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
