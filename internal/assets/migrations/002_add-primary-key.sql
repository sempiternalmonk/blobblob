-- +migrate Up

alter table blobs add primary key (blob_id);

-- +migrate Down

alter table blobs drop constraint blob_id;