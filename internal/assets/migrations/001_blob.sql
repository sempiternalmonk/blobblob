-- +migrate Up

create table blobs (
	blob_id serial,
	owner_address character(64) not null,
	json_blob jsonb default '{}'::jsonb not null
);

-- +migrate Down 

drop table blobs cascade;