package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	errs "gitlab.com/sempiternalmonk/blobblob/internal/service/errors"
	"gitlab.com/sempiternalmonk/blobblob/internal/service/requests"
	"gitlab.com/sempiternalmonk/blobblob/resources"
)

func GetBlobs(w http.ResponseWriter, r *http.Request) {
	req, err := requests.NewGetBlobsRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	result, err := BlobQ(r).SelectByUID(req.UserID)
	if err != nil {
		switch err.Error() {
		case "user not found":
			err = errs.NoUserIDDBEntry{UID: req.UserID}
			w.WriteHeader(http.StatusNotFound)
			ape.RenderErr(w, errs.NoUserIDDBEntryJSONapi(err)...)
			return
		default:
			ape.RenderErr(w, problems.BadRequest(err)...)
			return
		}
	}
	response := resources.BlobListResponse{}
	response.Data = make([]resources.Blob, 0, len(result))
	for _, entry := range result {
		strBlobId := strconv.Itoa(entry.BlobID)
		response.Data = append(response.Data, resources.Blob{
			Key: resources.Key{
				ID:   req.UserID,
				Type: resources.RECEIVE_BLOB,
			},
			Attributes: resources.BlobAttributes{
				BlobId: &strBlobId,
				Value:  json.RawMessage(entry.JsonBlob),
			},
		})
	}
	ape.Render(w, response)
}
