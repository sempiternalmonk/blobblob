package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/sempiternalmonk/blobblob/internal/data"
	"gitlab.com/sempiternalmonk/blobblob/internal/service/requests"
	"gitlab.com/sempiternalmonk/blobblob/resources"
)

func NewBlob(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewNewBlobRequest(r)
	if err != nil {
		Log(r).WithError(err).Info("invalid request")

		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	resultBlob, err := BlobQ(r).Insert(data.Blob{
		OwnerAddress: request.Data.ID,
		JsonBlob:     string(request.Data.Attributes.Value),
	})

	if err != nil {
		Log(r).WithError(err).Error(err.Error())
		ape.RenderErr(w, problems.InternalError())
		return
	}
	//stringBlobID := fmt.Sprintf("%d", resultBlob.BlobID)
	stringBlobID := strconv.Itoa(resultBlob.BlobID)

	result := resources.BlobResponse{
		Data: resources.Blob{
			Key: resources.Key{
				ID:   strings.Trim(resultBlob.OwnerAddress, " "),
				Type: resources.REGISTERED_BLOB,
			},
			Attributes: resources.BlobAttributes{
				BlobId: &stringBlobID,
				Value:  json.RawMessage(resultBlob.JsonBlob),
			},
		},
	}
	w.WriteHeader(http.StatusCreated)
	ape.Render(w, &result)
}
