package handlers

import (
	"net/http"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/sempiternalmonk/blobblob/internal/data"
	errs "gitlab.com/sempiternalmonk/blobblob/internal/service/errors"
	"gitlab.com/sempiternalmonk/blobblob/internal/service/requests"
)

func DeleteBlob(w http.ResponseWriter, r *http.Request) {
	req, err := requests.NewBlobIDRequest(r)
	if err != nil {
		ape.RenderErr(w, errs.InvalidParameterJSONapi(err)...)
		return
	}
	err = BlobQ(r).Transaction(func(q data.BlobQ) error {
		res, err := q.Delete(req.BlobID)
		if err != nil {
			return errors.Wrap(err, "Could not delete entry")
		}
		rows, err := res.RowsAffected()
		if err != nil {
			return errors.Wrap(err, "DB doesnt support some features")
		}
		if rows == 0 {
			return errs.NoBlobDBEntry{ID: req.BlobID}
		}
		return err
	})
	if err != nil {
		switch cause := errors.Cause(err); cause.(type) {
		case errs.NoBlobDBEntry:
			Log(r).Info("Blob not found")
			ape.RenderErr(w, errs.NoBlobDBEntryJSONapi(cause)...)
			return
		default:
			Log(r).WithError(err).Error("Could not delete blob")
			ape.RenderErr(w, problems.InternalError())
		}
	}
	w.WriteHeader(http.StatusNoContent)
}
