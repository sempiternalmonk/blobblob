package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/sempiternalmonk/blobblob/internal/data"
	errs "gitlab.com/sempiternalmonk/blobblob/internal/service/errors"
	"gitlab.com/sempiternalmonk/blobblob/internal/service/requests"
	"gitlab.com/sempiternalmonk/blobblob/resources"
)

func GetBlob(w http.ResponseWriter, r *http.Request) {
	req, err := requests.NewBlobIDRequest(r)
	if err != nil {
		ape.RenderErr(w, errs.InvalidParameterJSONapi(err)...)
		return
	}
	var result data.Blob

	result, err = BlobQ(r).GetByID(req.BlobID)
	if err != nil {
		// either way blob is not found, sql doesn't have good errors for handling
		err = errs.NoBlobDBEntry{ID: req.BlobID}
		ape.RenderErr(w, errs.NoBlobDBEntryJSONapi(err)...)
		return
	}
	stringBlobID := fmt.Sprintf("%d", result.BlobID)
	response := resources.BlobResponse{
		Data: resources.Blob{
			Key: resources.Key{
				ID:   strings.Trim(result.OwnerAddress, " "),
				Type: resources.RECEIVE_BLOB,
			},
			Attributes: resources.BlobAttributes{
				BlobId: &stringBlobID,
				Value:  json.RawMessage(result.JsonBlob),
			},
		},
	}
	ape.Render(w, &response)
}
