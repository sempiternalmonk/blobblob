package requests

import (
	"net/http"

	"github.com/go-chi/chi"
	validation "github.com/go-ozzo/ozzo-validation/v4"
)

type GetBlobsRequest struct {
	UserID string `url:"-"`
}

const uidQueryName = "address"

func NewGetBlobsRequest(r *http.Request) (GetBlobsRequest, error) {
	req := GetBlobsRequest{UserID: chi.URLParam(r, uidQueryName)}
	return req, validation.Errors{
		"address": validation.Validate(req.UserID, validation.Required,
			validation.Length(1, 64)),
	}.Filter()
}
