package requests

import (
	"encoding/json"
	"net/http"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/sempiternalmonk/blobblob/resources"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

type NewBlobRequest struct {
	Data resources.Blob
}

// naming is a bit poor
func NewNewBlobRequest(r *http.Request) (NewBlobRequest, error) {
	var req NewBlobRequest

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return req, errors.Wrap(err, "failed to unmarshal")
	}
	return req, req.validate()
}

func (r *NewBlobRequest) validate() error {
	return validation.Errors{
		"/data/id": validation.Validate(r.Data.ID, validation.Required,
			validation.Length(1, 64)),
		"/data/type":             validateType(r.Data.Type),
		"/data/attributes/value": validation.NotNil.Validate(r.Data.Attributes.Value),
	}.Filter()
}

func validateType(rt resources.ResourceType) error {

	rule := validation.In(resources.REGISTER_BLOB)
	return validation.Validate(rt, rule)
}
