package requests

import (
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	errs "gitlab.com/sempiternalmonk/blobblob/internal/service/errors"
)

// this type is created for handlers, which require blob id only
// those are: delete and get requests
type BlobIDRequest struct {
	BlobID int32 `url:"-"`
}

const idQueryName = "id"

func NewBlobIDRequest(r *http.Request) (BlobIDRequest, error) {
	req := BlobIDRequest{}
	val := chi.URLParam(r, idQueryName)
	i, err := strconv.Atoi(val)
	if err != nil {
		return req, &errs.InvalidParameter{
			ParameterName: idQueryName,
			PassedValue:   val,
		}
	}
	req.BlobID = int32(i)
	return req, nil
}
