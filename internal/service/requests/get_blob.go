package requests

import (
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	errs "gitlab.com/sempiternalmonk/blobblob/internal/service/errors"
)

type GetBlobRequest struct {
	BlobID int32 `url:"-"`
}

//const idQueryName = "id"

// returns intended from request blob id
// replaced with NewBlobIDRequest
func NewGetBlobRequest(r *http.Request) (GetBlobRequest, error) {
	req := GetBlobRequest{}
	val := chi.URLParam(r, idQueryName)
	i, err := strconv.Atoi(val)
	if err != nil {
		return req, &errs.InvalidParameter{
			ParameterName: idQueryName,
			PassedValue:   val,
		}
	}
	req.BlobID = int32(i)
	return req, nil
}
