package service

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"

	"github.com/go-chi/chi"
	"github.com/magiconair/properties/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/sempiternalmonk/blobblob/internal/data"
	mock_data "gitlab.com/sempiternalmonk/blobblob/internal/data/mocks"
	errs "gitlab.com/sempiternalmonk/blobblob/internal/service/errors"
	"gitlab.com/sempiternalmonk/blobblob/internal/service/handlers"
	"gitlab.com/sempiternalmonk/blobblob/internal/service/requests"
	"gitlab.com/sempiternalmonk/blobblob/resources"
)

const contentType = "application/json"

var logctx *logan.Entry = logan.New()

// add routes outside
func createRouter(storage *mock_data.BlobQ) *chi.Mux {
	r := chi.NewRouter()
	r.Use(
		ape.CtxMiddleware(
			handlers.CtxLog(logctx),
			handlers.CtxBlobQ(storage),
		),
	)

	return r
}

// you should close response outside
func httpRequest(url string, method string, contentType string, body io.Reader) (*http.Response, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, fmt.Errorf("Could not initialise request: %s", err.Error())
	}
	if contentType != "" {
		req.Header.Set("Content-Type", strings.Join([]string{contentType, "charset=UTF-8"}, "; "))
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("Could not get response: %s", err.Error())
	}
	return resp, nil

}

func TestNewBlobHandler(t *testing.T) {
	storage := mock_data.BlobQ{}

	router := createRouter(&storage)
	router.Post("/newblob", handlers.NewBlob)

	server := httptest.NewServer(router)
	defer server.Close()

	postBody := requests.NewBlobRequest{
		Data: resources.Blob{
			Key: resources.Key{
				ID:   "testing-id",
				Type: resources.REGISTER_BLOB,
			},
			Attributes: resources.BlobAttributes{
				Value: json.RawMessage(`{"pass-db":[{"gmail":"password"},{"hotmail":"otherpass"}]}`),
			},
		},
	}
	storage.On("New").Return(&storage)
	storage.On("Insert", data.Blob{
		OwnerAddress: postBody.Data.Key.ID,
		JsonBlob:     string(postBody.Data.Attributes.Value),
	}).Return(data.Blob{
		OwnerAddress: "testing-id",
		BlobID:       1,
		JsonBlob:     `{"pass-db":[{"gmail":"password"},{"hotmail":"otherpass"}]}`,
	}, nil)

	marshaled, err := json.Marshal(postBody)
	if err != nil {
		t.Fatal("Could not marshal body")
	}

	url := strings.Join([]string{server.URL, "newblob"}, "/")

	//resp, err := http.Post(url, contentType, bytes.NewBuffer(marshaled))
	resp, err := httpRequest(url, "POST", contentType, bytes.NewBuffer(marshaled))
	if err != nil {
		t.Fatal(err.Error())
	}
	defer resp.Body.Close()
	assert.Equal(t, resp.StatusCode, 201)
}

func TestNewBlobHandler_InvalidType(t *testing.T) {
	storage := mock_data.BlobQ{}
	router := createRouter(&storage)
	router.Post("/newblob", handlers.NewBlob)

	server := httptest.NewServer(router)
	defer server.Close()

	postBody := requests.NewBlobRequest{
		Data: resources.Blob{
			Key: resources.Key{
				ID:   "testing-id",
				Type: "test-invalid-type",
			},
			Attributes: resources.BlobAttributes{
				Value: json.RawMessage(`{"pass-db":[{"gmail":"password"},{"hotmail":"otherpass"}]}`),
			},
		},
	}

	marshaled, err := json.Marshal(postBody)
	if err != nil {
		t.Fatal("Could not marshal body")
	}
	url := strings.Join([]string{server.URL, "newblob"}, "/")
	resp, err := httpRequest(url, "POST", contentType, bytes.NewBuffer(marshaled))
	if err != nil {
		t.Fatal(err.Error())
	}
	defer resp.Body.Close()
	assert.Equal(t, resp.StatusCode, 400)
}

func TestGetBlobHandler(t *testing.T) {
	storage := mock_data.BlobQ{}
	storage.On("New").Return(&storage)
	storage.On("GetByID", int32(100)).Return(data.Blob{
		OwnerAddress: "testing-id",
		BlobID:       10,
		JsonBlob:     `{"key":"value"}`,
	}, nil)

	router := createRouter(&storage)
	router.Get("/getblob/{id}", handlers.GetBlob)

	server := httptest.NewServer(router)
	defer server.Close()

	url := strings.Join([]string{server.URL, "getblob/100"}, "/")
	resp, err := http.Get(url)
	if err != nil {
		t.Fatalf("Couldn't fetch URL: %s", err.Error())
	}
	defer resp.Body.Close()
	assert.Equal(t, resp.StatusCode, 200)
}

func TestGetBlobHandler_InvalidID(t *testing.T) {
	storage := mock_data.BlobQ{}
	storage.On("New").Return(&storage)
	storage.On("GetByID", int32(100)).Return(data.Blob{}, errors.New("not found"))
	router := createRouter(&storage)
	router.Get("/getblob/{id}", handlers.GetBlob)

	server := httptest.NewServer(router)
	defer server.Close()

	url := strings.Join([]string{server.URL, "getblob/100"}, "/")
	resp, err := http.Get(url)
	if err != nil {
		t.Fatalf("Couldn't fetch URL: %s", err.Error())
	}
	defer resp.Body.Close()
	assert.Equal(t, resp.StatusCode, 404)
}

func TestGetBlobHandler_InvalidInput(t *testing.T) {
	storage := mock_data.BlobQ{}
	// storage.On("New").Return(&storage)
	// storage.On("GetByID", int32(100)).Return(data.Blob{}, errors.New("not found"))
	router := createRouter(&storage)
	router.Get("/getblob/{id}", handlers.GetBlob)

	server := httptest.NewServer(router)
	defer server.Close()

	url := strings.Join([]string{server.URL, "getblob/nonsense"}, "/")
	resp, err := http.Get(url)
	if err != nil {
		t.Fatalf("Couldn't fetch URL: %s", err.Error())
	}
	defer resp.Body.Close()
	assert.Equal(t, resp.StatusCode, 400)
}

func TestGetBlobsHandler(t *testing.T) {
	uid := "user-id"
	storage := mock_data.BlobQ{}
	storage.On("New").Return(&storage)
	storage.On("SelectByUID", uid).Return([]data.Blob{
		{
			OwnerAddress: uid,
			BlobID:       1,
			JsonBlob:     `{"param":"value"}`,
		},
		{
			OwnerAddress: uid,
			BlobID:       2,
			JsonBlob:     `{"param1":"value1"}`,
		},
	}, nil) // error

	router := createRouter(&storage)
	router.Get("/getblobs/{address}", handlers.GetBlobs)

	server := httptest.NewServer(router)
	defer server.Close()

	url := strings.Join([]string{server.URL, "getblobs/" + uid}, "/")
	resp, err := http.Get(url)
	if err != nil {
		t.Fatalf("Couldn't fetch URL: %s", err.Error())
	}
	defer resp.Body.Close()
	assert.Equal(t, resp.StatusCode, http.StatusOK)
}

func TestGetBlobsHandler_NonExistingUID(t *testing.T) {
	uid := "user-id"
	storage := mock_data.BlobQ{}
	storage.On("New").Return(&storage)
	storage.On("SelectByUID", uid).Return([]data.Blob{}, errors.New("user not found")) // error

	router := createRouter(&storage)
	router.Get("/getblobs/{address}", handlers.GetBlobs)

	server := httptest.NewServer(router)
	defer server.Close()

	url := strings.Join([]string{server.URL, "getblobs/" + uid}, "/")
	resp, err := http.Get(url)
	if err != nil {
		t.Fatalf("Couldn't fetch URL: %s", err.Error())
	}
	defer resp.Body.Close()
	assert.Equal(t, resp.StatusCode, http.StatusNotFound)
}

func TestGetBlobsHandler_InvalidInput(t *testing.T) {
	uid := strings.Repeat("a", 65)
	storage := mock_data.BlobQ{}
	// storage.On("New").Return(&storage)
	// storage.On("SelectByUID", uid).Return([]data.Blob{}, errors.New("user not found")) // error

	router := createRouter(&storage)
	router.Get("/getblobs/{address}", handlers.GetBlobs)

	server := httptest.NewServer(router)
	defer server.Close()

	url := strings.Join([]string{server.URL, "getblobs/" + uid}, "/")
	resp, err := http.Get(url)
	if err != nil {
		t.Fatalf("Couldn't fetch URL: %s", err.Error())
	}
	defer resp.Body.Close()
	assert.Equal(t, resp.StatusCode, http.StatusBadRequest)
}

/*
type mock_sqlResult struct {
	//mock.Mock
	rawsAffected int64
	lastInsertId int64
}

func (msr *mock_sqlResult) LastInsertId() (int64, error) {
	return msr.lastInsertId, nil
}

func (msr *mock_sqlResult) RowsAffected() (int64, error) {
	return int64(msr.rawsAffected), nil
}
*/

func TestDeleteBlob(t *testing.T) {
	id := 1
	storage := mock_data.BlobQ{}
	storage.On("New").Return(&storage)
	storage.On("Transaction", mock.AnythingOfType("func(data.BlobQ) error")).Return(nil)

	//mockSqlResult := mock_sqlResult{rawsAffected: 1, lastInsertId: -1}
	//storage.On("Delete", id).Return(&mockSqlResult)

	router := createRouter(&storage)

	router.Delete("/deleteblob/{id}", handlers.DeleteBlob)

	server := httptest.NewServer(router)
	defer server.Close()

	url := strings.Join([]string{server.URL, "deleteblob/" + strconv.Itoa(id)}, "/")
	resp, err := httpRequest(url, "DELETE", "", nil)
	if err != nil {
		t.Fatalf("Couldn't fetch URL: %s", err.Error())
	}
	defer resp.Body.Close()
	assert.Equal(t, resp.StatusCode, http.StatusNoContent)
}

func TestDeleteBlob_InvalidID(t *testing.T) {
	storage := mock_data.BlobQ{}
	storage.On("New").Return(&storage)
	storage.On("Transaction", mock.AnythingOfType("func(data.BlobQ) error")).Return(nil)

	router := createRouter(&storage)

	router.Delete("/deleteblob/{id}", handlers.DeleteBlob)

	server := httptest.NewServer(router)
	defer server.Close()

	url := strings.Join([]string{server.URL, "deleteblob/nonsense"}, "/")
	resp, err := httpRequest(url, "DELETE", "", nil)
	if err != nil {
		t.Fatalf("Couldn't fetch URL: %s", err.Error())
	}
	defer resp.Body.Close()
	assert.Equal(t, resp.StatusCode, http.StatusBadRequest)
}
func TestDeleteBlob_NoBlob(t *testing.T) {
	var id int32 = 404
	storage := mock_data.BlobQ{}
	storage.On("New").Return(&storage)
	storage.On("Transaction", mock.AnythingOfType("func(data.BlobQ) error")).
		Return(errs.NoBlobDBEntry{ID: id})

	router := createRouter(&storage)

	router.Delete("/deleteblob/{id}", handlers.DeleteBlob)

	server := httptest.NewServer(router)
	defer server.Close()

	url := strings.Join([]string{server.URL, "deleteblob/" + fmt.Sprint(id)}, "/")
	resp, err := httpRequest(url, "DELETE", "", nil)
	if err != nil {
		t.Fatalf("Couldn't fetch URL: %s", err.Error())
	}
	defer resp.Body.Close()
	assert.Equal(t, resp.StatusCode, http.StatusNotFound)
}
