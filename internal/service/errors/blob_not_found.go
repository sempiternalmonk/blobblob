package errors

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/google/jsonapi"
	"gitlab.com/distributed_lab/ape/problems"
)

type NoBlobDBEntry struct {
	ID int32
}

func NoBlobDBEntryJSONapi(err error) []*jsonapi.ErrorObject {
	//fmt.Printf("%T", err)
	switch err.(type) {
	case *NoBlobDBEntry:
		return []*jsonapi.ErrorObject{
			{
				Title:  http.StatusText(http.StatusNotFound),
				Status: strconv.Itoa(http.StatusNotFound),
				Detail: err.Error(),
			},
		}
	case NoBlobDBEntry:
		return []*jsonapi.ErrorObject{
			{
				Title:  http.StatusText(http.StatusNotFound),
				Status: strconv.Itoa(http.StatusNotFound),
				Detail: err.Error(),
			},
		}
	default:
		return problems.BadRequest(err)
	}
}

func (err NoBlobDBEntry) Error() string {
	return fmt.Sprintf("Blob with id %d not found", err.ID)
}
