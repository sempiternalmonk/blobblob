// user id not found
package errors

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/google/jsonapi"
	"gitlab.com/distributed_lab/ape/problems"
)

type NoUserIDDBEntry struct {
	UID string
}

func NoUserIDDBEntryJSONapi(err error) []*jsonapi.ErrorObject {
	//fmt.Printf("%T", err)
	switch err.(type) {
	case *NoUserIDDBEntry:
		return []*jsonapi.ErrorObject{
			{
				Title:  http.StatusText(http.StatusBadRequest),
				Status: strconv.Itoa(http.StatusBadRequest),
				Detail: err.Error(),
			},
		}
	default:
		return problems.BadRequest(err)
	}
}

func (err NoUserIDDBEntry) Error() string {
	return fmt.Sprintf("No blobs with user ID `%s` found", err.UID)
}
