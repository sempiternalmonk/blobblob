package errors

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/google/jsonapi"
)

type InvalidParameter struct {
	ParameterName string
	PassedValue   string
}

func InvalidParameterJSONapi(ip error) []*jsonapi.ErrorObject {
	switch ip.(type) {
	case *InvalidParameter:
		return []*jsonapi.ErrorObject{
			{
				Title:  http.StatusText(http.StatusBadRequest),
				Status: strconv.Itoa(http.StatusBadRequest),
				Detail: ip.Error(),
			},
		}
	default:
		panic("unimplemented other variants in invalid parameter")
	}
}

func (ip InvalidParameter) Error() string {
	return fmt.Sprintf("Can not use `%s` as param in %s", ip.PassedValue, ip.ParameterName)
}
