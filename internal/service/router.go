package service

import (
	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/sempiternalmonk/blobblob/internal/data/postgre"
	"gitlab.com/sempiternalmonk/blobblob/internal/service/handlers"
)

func (s *service) router() chi.Router {
	r := chi.NewRouter()

	r.Use(
		ape.RecoverMiddleware(s.log),
		ape.LoganMiddleware(s.log),
		ape.CtxMiddleware(
			handlers.CtxLog(s.log),
			handlers.CtxBlobQ(postgre.NewBlobsQ(s.db)),
		),
	)
	// configure endpoints here
	r.Route("/deleteblob", func(r chi.Router) {
		r.Delete("/{id}", handlers.DeleteBlob)
	})
	r.Route("/getblob", func(r chi.Router) {
		r.Get("/{id}", handlers.GetBlob)
	})
	r.Route("/getblobs", func(r chi.Router) {
		r.Get("/{address}", handlers.GetBlobs)
	})
	r.Post("/newblob", handlers.NewBlob)

	return r
}
